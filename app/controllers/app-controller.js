app.controller('AppCtrl', function($scope, $window, httpService) {
    // variable declarations
    $scope.startDate = new Date();
    $scope.tennantName = '';

    // function declarations
    $scope.reserveRoom = reserveRoom;
    $scope.getReservedRooms = getReservedRooms;

    function reserveRoom(reserve) {
        const body = {
            tennantName: $scope.tennantName,
            time: Math.floor($scope.startDate.getTime()/1000),
            reserved: reserve
        };
        httpService.reserveRoom(body)
            .then((response) => {
                $scope.tennantName = '';
                if (response.success) {
                    $window.alert(reserve ? "Stay Confirmed Successfully" : 'Stay Cancelled Succefully')
                }
            }).catch((e) => {
            $window.alert(e.data)
        })
    }

    function getReservedRooms() {
        let startDate = $scope.startDate;
        // convert this to start of the day for making range, because I am not clear about how to get range
        startDate.setHours(1,0,0,0);
        httpService.getReservedRooms(Math.floor(startDate.getTime()/1000), Math.floor(new Date().getTime()/1000))
            .then((response) => {
                if (response.success) {
                    if (response.data.reserved.length > 0)
                        $window.alert('Slot is not available')
                    else
                        $window.alert('Slot is available')
                }
            }).catch((e) => {
            $window.alert(e.data)
        })
    }
});
