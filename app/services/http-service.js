app.service('httpService', function ($http,$q) {

    return {
        reserveRoom: reserveRoom,
        getReservedRooms: getReservedRooms,
    };

    function reserveRoom(body) {
        var defer = $q.defer();
        $http({
            method : 'POST',
            url : 'http://localhost:3000/reserve',
            data: body
        }).then(function (response) {
            defer.resolve({
                success : true,
                data : response.data
            })
        }).catch((e) => {
            defer.reject({
                success : false,
                data : e.data
            })
        });
        return defer.promise;
    }

    function getReservedRooms(startDate, endDate) {
        var defer = $q.defer();
        $http({
            method : 'GET',
            url : `http://localhost:3000/reserve/${startDate}/${endDate}`,
        }).then(function (response) {
            defer.resolve({
                success : true,
                data : response.data
            })
        }).catch((e) => {
            defer.reject({
                success : false,
                data : e.data
            })
        });
        return defer.promise;
    }
});
